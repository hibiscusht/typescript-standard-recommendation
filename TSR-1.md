**PENDAHULUAN**


1. Rekomendasi umum dokumentasi
2. Rekomendasi keterangan TSR


**UMUM**


Seluruh bagian syntax project Typescript wajib memiliki dokumentasi dengan mengikuti standar JSDoc untuk [Typescript](https://www.typescriptlang.org/docs/handbook/jsdoc-supported-types.html). Format dokumentasi dengan block comment sebagai berikut


```typescript
/**
 *
 *
 */
```


Di bagian paling atas dari setiap file diberikan dokumentasi dengan tag `@author` diikuti nama pengembang lalu enter dan diikuti dengan tag @version. Jika satu file diupdate oleh lebih dari satu pengembang, maka dokumentasi nama pengembang ditulis urut version nya.



```typescript
/**
 *@author John Doe
 *@version 1.0
 */
/**
 *@author Jane Doe
 *@version 1.1
 */
```
 
Susunan untuk dokumentasi adalah sebagai berikut



```typescript
/**
 * deskripsi
 *
 * jenis elemen
 * nama elemen
 * parameter
 * return
 */
```


|Komponen Dokumentasi  |Keterangan                          |
|----------------------|------------------------------------|
|deskripsi             |penjelasan singkat kegunaan elemen  |
|jenis elemen          |`@interface, @typedef, @class,`     |
|                      |`@method, @property, @constant`     |
|nama elemen           |nama elemen dengan tag `@name`      |
|parameter             |`@param {tipe data} nama parameter` |
|return                |`@return {tipe data}`               | 


> [!TIP]
> Catatan
>
> Untuk method asynchronous, sebelum jenis elemen `@method`, tambahkan tag `@async`
> Untuk decorator, jenis elemen nya adalah `@function`

> [!TIP]
> Catatan
>
> Untuk nama elemen mengikuti [TSR-0](TSR-0.md) untuk variable constant dan untuk elemen selain
> variable mengikuti [TSR-3](TSR-3.md)