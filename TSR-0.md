**PENDAHULUAN**


1. Rekomendasi untuk penamaan file
2. Rekomendasi untuk penggunaan semicolon
3. Rekomendasi untuk penggunaan baris
4. Rekomendasi untuk penggunaan annotation
5. Rekomendasi untuk definisi variable
6. Rekomendasi untuk iterasi
7. Rekomendasi untuk decorator


**PENAMAAN FILE**


Nama file berakhiran `.ts` untuk Typescript standar dan `.tsx` untuk Typescript dengan React, seluruh nya dalam huruf kecil tanpa karakter khusus selain tanda dash (-). Setiap nama file menggambarkan kegunaan nya. Jika terdiri atas dua (2) kata maka dipisahkan dengan tanda dash.


Setiap project hanya memiliki satu (1) file bernama `index.ts` yang akan digunakan di dalam Node.js atau diimportkan ke dalam browser. File ini sebagai inisial dari seluruh project.


File-file dengan kegunaan sama dikumpulkan dalam satu folder dan diberi nama sesuai rekomendasi di atas.


`controllers/user-controller.ts`

`models/user-model.ts`


**PENGGUNAAN SEMICOLON**


Typescript mendukung fitur *automatic semicolon insertion* yang bisa dimanfaatkan untuk memudahkan penulisan syntax. Semicolon *tidak perlu* diketikkan di setiap akhir definisi variable. Untuk menghindari kesalahan pengenalan object `document` di browser, maka setiap object `document` dimasukkan ke dalam definisi variable.



```typescript
const user_name: string = (<HTMLInputElement>document.querySelector('#user_name')).value
const button = (<HTMLButtonElement>document.querySelector('#btn-ok'))
```


**PENGGUNAAN BARIS**


Setiap baris hanya berisi satu (1) definisi panjang nya maksimal 80 karakter. Jika penggunaan annotation dan interface menyebabkan satu definisi melebihi 80 karakter maka biarkan terjadi soft return.


Baris di kolom satu (1) hanya berisi definisi class, interface, type, import, dan export. Baris di dalam definisi di atas diberi indent sejajar dengan nama definisi.



```typescript
import Loremipsum from "./loaded"
interface Lorem {
          data_base: string
          Data(): string
}
type CustomType = {
     name: string
}
class Loremi {
      public my_data: string = ''
      private async getData(): Promise<void>
      {
                    const tgl = new Date()
      }
}
export default Loremi
export { Lorem, CustomType }
```


**PENGGUNAAN ANNOTATION**


Setiap annotation diawali dengan tanda colon (:) diikuti satu (1) spasi diikuti tipe data diikuti satu (1) spasi diikuti tanda sama dengan (=) diikuti nilai. Annotation tidak digunakan jika definisi variable berupa object


**DEFINISI VARIABLE**


Definisi variable yang dianjurkan adalah `const` dan `let`. Definisi const digunakan untuk nilai yang tidak berubah sekali didefinisikan seperti object atau implementasi rumus. Definisi let digunakan untuk nilai yang sering berubah.


Nama variable semuanya dalam huruf kecil tanpa karakter khusus selain tanda underscore (_).



```typescript
let hitung: number = 0
const button = (<HTMLButtonElement>document.querySelector('#btn-ok'))
const total: number = harga_barang * jumlah_barang
const re = new RegExp('[a-z]','gi')
```


**ITERASI**


Dalam hal melakukan iterasi yang memerlukan conditional dan `break` digunakan statement `for`, `for ... of`, dan `while`. Sedangkan untuk data Array digunakan method iterasi yang tersedia. 


Penulisan masing-masing statement adalah dengan menambahkan spasi setelah kata for dan while serta setelah tanda kurung.


```typescript
class Usercontroller {
      public async getData(): Promise<string>
      {
                  const data_user = await Usermodel.findAll()
                  for (const row of data_user) {
                      row.userAge = row.Age * 2.5
                  }
      }
}
```


**DECORATOR**


Decorator diletakkan di dalam file tersendiri yang diberi nama sesuai target class nya.

`decorators/user-decorator.ts`

Di dalam file decorator terdapat function untuk masing-masing decorator: class, method, property, argument. Masing-masing function decorator diekspor secara terpisah dengan *named export* dan diberi comment sesuai [TSR-1](TSR-1.md). Penamaan function decorator diawali huruf kecil, dilanjutkan dengan huruf kapital di kata berikut nya, tanpa spasi dan karakter khusus.



```typescript
/**
 * Description
 * 
 * @function
 * @name decorateUserClass  
 * @param {T} constructor
 * @returns {T}
 */
function decorateUserClass<T extends { new (...args: any[]): any }>(constructor: T): T {

}
export { decorateUserClass }
```



   