**SELAMAT DATANG DI TYPESCRIPT STANDARD RECOMMENDATION**


Typescript Standard Recommendation (TSR) adalah suatu kumpulan tata cara menuliskan script Typescript (**syntax**) yang bersifat komunikatif, produktif, dengan skalabilitas yang tinggi.


**Daftar Isi**


TSR-0 rekomendasi untuk penamaan file dan syntax umum

TSR-1 rekomendasi untuk JSDoc

TSR-2 rekomendasi untuk module

TSR-3 rekomendasi untuk OOP


**Kontribusi**


Bagi anda yang ingin berkontribusi, silakan baca terlebih dahulu TSR yang sudah ada, lalu copy kan isi TSR tersebut ke file markdown baru dan beri nomor TSR sesuai urutan berikut nya. Tuliskan comment di awal file markdown tersebut TSR mana yang diacu. Comment ini harus mengikuti rekomendasi [TSR-1](TSR-1.md)


TSR baru yang diterima, maka TSR lama akan ditandai `deprecated`
